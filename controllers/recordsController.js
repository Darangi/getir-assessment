const { logger } = require('../utils/logger')
const recordsService = require('../services/records')
/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
exports.getRecords = async (req, res) => {
  try {
    const { startDate, endDate, minCount, maxCount } = req.body
    const records = await recordsService.getRecords({ startDate, endDate, minCount, maxCount })

    logger.debug(`${records.length} record(s) fetched successfully`)

    return res.send({ code: 0, msg: 'Success', records, })
  }
  catch (ex) {
    logger.error(`Failed to fetch records ${ex.message}`)
    
    return res.status(500).send({ code: 2, msg: 'An internal error occured' })
  }
}