const Server = require('../script/server')

describe('Server', () => {
  let server
  const serverHelper = () => {
    const port = process.env.PORT || ''
    const databaseURL = process.env.TEST_DATABASE_URL || ''

    server = new Server({
      databaseURL,
      port
    })
    return server
  }
  
  it('Should return an error message when an incorrect db connection and string is passed', async () => {
    const server = serverHelper()
    const message = await server.start()

    expect(message).toMatch(`Failed to start server`)
  })

  it('Should start the server successfully', async () => {
    require('dotenv').config()
    
    const server = serverHelper()
    const message = await server.start()

    expect(message).toMatch(`Server started on PORT`)
    await server.stop()
  })

})