const getRecordsMiddleWare = require('../middleware/getRecords')

describe('Middleware', () => {
  const validatorHelper = (fields) => {
    const { error } = getRecordsMiddleWare(fields)
    return error ? error.details[0].message : null
  }

  it('Should return an error message when endDate is missing', () => {
    const fields = {
      startDate: '2016-01-26', minCount: 2700, maxCount: 3000
    }
    expect(validatorHelper(fields)).toContain('endDate')
  })

  it('Should return an error message when startDate is missing', () => {
    const fields = {
      endDate: '2016-01-26', minCount: 2700, maxCount: 3000
    }
    expect(validatorHelper(fields)).toContain('startDate')
  })

  it('Should return an error message when minCount is missing', () => {
    const fields = {
      startDate: '2016-01-26', endDate: '2016-01-26', maxCount: 3000
    }
    expect(validatorHelper(fields)).toContain('minCount')
  })

  it('Should return an error message when maxCount is missing', () => {
    const fields = {
      startDate: '2016-01-26', endDate: '2016-01-26', minCount: 2700
    }
    expect(validatorHelper(fields)).toContain('maxCount')
  })

  it('Should return no error when all the fields are provided', () => {
    const fields = {
      startDate: '2016-01-26', endDate: '2016-01-26', minCount: 2700, maxCount: 3000
    }
    expect(validatorHelper(fields)).toBeNull()
  })

  it('Should return an error message when minCount is greater than maxCount', () => {
    const fields = {
      startDate: '2016-01-26', endDate: '2016-01-26', minCount: 3000, maxCount: 2700
    }
    expect(validatorHelper(fields)).toMatch(`must be greater than ${fields.minCount}`)
  })

  it('Should return an error message when an invalid date format is passed', () => {
    const fields = {
      startDate: '01-26-2016', endDate: '06-01-2000', minCount: 3000, maxCount: 2700
    }
    expect(validatorHelper(fields)).toMatch(`must be a string with one of the following formats [YYYY-MM-DD]`)
  })
})