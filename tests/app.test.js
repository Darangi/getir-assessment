require('dotenv').config()

const Server = require('../script/server')
const request = require('supertest')


describe('/api/v1/records', () => {
  const baseURL = '/api/v1/records'

  jest.setTimeout(30000)

  const app = new Server({
    databaseURL: process.env.DATABASE_URL,
    port: process.env.PORT
  })

  const post = async (payload) => {
    return request(app.server).post(baseURL).send(payload)
  }

  beforeAll(async () => {
    await app.start()
  })

  afterAll(async () => {
    await app.stop()
  })

  it('Should return 400 http code when query param validation fails', async () => {
    const res = await post({
      startDate: '06-01-2006',
      endDate: '2018-02-02',
      minCount: 3000,
      maxCount: 4000
    })
    expect(res.status).toBe(400)
  })

  it('Should return 200 http code', async () => {
    const res = await post({
      startDate: '2016-01-26',
      endDate: '2018-02-02',
      maxCount: 3000,
      minCount: 2700
    })
    expect(res.status).toBe(200)
  })

  it('Should return records between 2016-01-26 and 2018-02-02', async () => {
    const res = await post({
      startDate: '2016-01-26',
      endDate: '2018-02-02',
      minCount: 2700,
      maxCount: 3000
    })
    const startDate = new Date('2016-01-26').getTime()
    const endDate = new Date('2018-02-02').getTime()
    for (let record of res.body.records) {
      expect(new Date(record.createdAt).getTime()).toBeGreaterThanOrEqual(startDate)
      expect(new Date(record.createdAt).getTime()).toBeLessThanOrEqual(endDate)
    }
  })

  it('Should return records that have totalCount between 2700 and 3000', async () => {
    const res = await post({
      startDate: '2016-01-26',
      endDate: '2018-02-02',
      minCount: 2700,
      maxCount: 3000
    })
    for (let record of res.body.records) {
      expect(record.totalCount).toBeGreaterThanOrEqual(2700)
      expect(record.totalCount).toBeLessThanOrEqual(3000)
    }
  })
})
