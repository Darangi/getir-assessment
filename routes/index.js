const express = require('express')
const recordsRoute = require('./records')
const router = express.Router()

router.use('/records', recordsRoute)

module.exports = router
