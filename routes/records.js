const express = require('express')
const router = express.Router()

const recordsController = require('../controllers/recordsController')

const validator = require('../middleware/validator')
const getRecordsMiddleWare = require('../middleware/getRecords')

router.post('/', validator(getRecordsMiddleWare), recordsController.getRecords)

module.exports = router