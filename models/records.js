const mongoose = require('mongoose')
const Schema = mongoose.Schema

const RecordsSchema = new Schema({}, { strict: false })
const Records = mongoose.model('Records', RecordsSchema, 'records')

module.exports = Records 