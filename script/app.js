
const express = require('express');
const bodyParser = require('body-parser')
const cors = require('cors')
const app = express();

const route = require("../routes")


app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cors());

app.use('/api/v1', route)

module.exports = app
