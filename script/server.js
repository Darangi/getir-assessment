const mongoose = require('mongoose')

const app = require('./app')
const { logger } = require('../utils/logger')

/**
 * This class helps to start and stop the server
 */
module.exports =
  class Server {
    /**
     * 
     * @param {object} options - contains config necessary to spin up our server
     */
    constructor(options) {
      this.databaseURL = options.databaseURL
      this.port = options.port
    }
    /**
     * helps connect mongodb and start the server on the specified port
     */
    async start() {
      try {
        logger.debug(`connecting to database: ${this.databaseURL}`)
        await mongoose.connect(this.databaseURL, { useNewUrlParser: true })
        logger.debug(`connecting to database ${this.databaseURL} successfull!`)

        return new Promise((resolve) => {
          logger.debug(`starting server at PORT:${this.port}`)
          this.server = app.listen(this.port)
          logger.debug(`Server started PORT:${this.port}`)

          resolve(`Server started on PORT:${this.port}`)
        })
      }
      catch (ex) {
        const error = `Failed to start server: ${ex.message}`
        logger.error(error)
        return error
      }
    }
    /**
    * helps stop currently running server
    */
    stop() {
      try {
        return new Promise((resolve) => {
          logger.debug(`Stopping server listening on port: ${this.port}`)
          this.server.close(resolve)
          logger.debug(`Server listening on port: ${this.port} stopped successfully`)
        })
      }
      catch (ex) {
        const error = `Failed to stop server:${ex.message}`
        logger.error(error)
        return error
      }
    }
  }