require('dotenv').config()

const Server = require('./script/server')

const server = new Server({databaseURL: process.env.DATABASE_URL, port: process.env.PORT})

server.start()

