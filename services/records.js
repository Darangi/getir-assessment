
const RecordsModel = require('../models/records')
/**
 * 
 * @param {date} startDate 
 * @param {date} endDate 
 * @param {number} minCount 
 * @param {number} maxCount 
 */
const getRecords = ({ startDate, endDate, minCount, maxCount }) => {
  const query = [
    {
      '$match': {
        'createdAt': {
          '$gte': new Date(startDate),
          '$lte': new Date(endDate)
        }
      }
    }, {
      '$addFields': {
        'totalCount': {
          '$reduce': {
            'input': '$counts',
            'initialValue': 0,
            'in': {
              '$add': [
                '$$value', '$$this'
              ]
            }
          }
        }
      }
    },
    {
      '$match': {
        'totalCount': {
          '$gte': +minCount,
          '$lte': +maxCount,
        }
      }
    }
  ]
  // queries mongodb and returned selected fields only (key, totalCount, createdAt)
  return RecordsModel.aggregate(query).project({ key: 1, totalCount: 1, createdAt: 1, _id: 0 })
}

module.exports = {
  getRecords
}
