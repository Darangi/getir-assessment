const { logger } = require('../utils/logger')

/**
 * Helps to validate parameters in the http request body
 * @param {object} validator - takes in a joi validator instance 
 */
module.exports = (validator) => {
  return (req, res, next) => {
    logger.debug(`${req.url} reached`)

    const { error } = validator(req.body)
    if (error) {
      const msg = error.details[0].message
      
      logger.debug(`Request failed: ${msg}`)
      
      return res.status(400).send({ code: 1, msg })
    }
    next()
  }
}
