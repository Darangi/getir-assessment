const date = require('joi-date-extensions')
const Joi = require('joi').extend(date)

/**
 * 
 * @param {object} fields - an object containing the parameters needed to make a valid http request
 */
module.exports = (fields) => {
  const schema =Joi.object({
    startDate: Joi.date().format('YYYY-MM-DD').required(),
    endDate: Joi.date().format('YYYY-MM-DD').required(),
    minCount: Joi.number().integer().required(),
    maxCount: Joi.number().integer().greater(Joi.ref('minCount')).required(),
  })

  return Joi.validate({
    startDate: fields.startDate,
    endDate: fields.endDate,
    minCount: fields.minCount,
    maxCount: fields.maxCount,
  }, schema)
}
