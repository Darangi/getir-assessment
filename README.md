# Environment variables
create an .env file based of .env-example and add your values to the environment variable
# Start the app
cd getir-assessment -> npm install-> npm start
# Run tests on node app
cd getir-assessment -> npm install-> npm run test
# Further info
custom code is returned in the body of the request: 
0 = request was successfull,
1 = validation of the query parameters failed and
2 = an error on the server occured
# Postman collection
https://documenter.getpostman.com/view/5376812/TVzREd5J